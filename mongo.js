// Aggregation in MongoDB

db.fruits.insertMany([
	{
		"name" : "Apple",
		"supplier" : "Red Farms Inc.",
		"stocks" : 20,
		"price" : 40,
		"onSale" : true
	},
	{
		"name" : "Banana",
		"supplier" : "Yellow Farms",
		"stocks" : 15,
		"price" : 20,
		"onSale" : true
	},
	{
		"name" : "Kiwi",
		"supplier" : "Green Farming and Canning",
		"stocks" : 25,
		"price" : 50,
		"onSale" : true
	},
	{
		"name" : "Mango",
		"supplier" : "Yellow Farms",
		"stocks" : 10,
		"price" : 60,
		"onSale" : true
	},
	{
		"name" : "Dragon Fruit",
		"supplier" : "Red Farm Inc.",
		"stocks" : 10,
		"price" : 60,
		"onSale" : true
	},
]);

// Aggregation in MongoDB
	// This is the act or process of generating manipulated data and perform operations to create filtered results that helps in data analysis.
	// This helps in creating reports from analyzing the data provided in our documents
	// This helps in arriving at summarized information NOT previously shown in our documents

	// Aggregation Pipelines
		// Aggregation is done in 2 to 3 steps. Each stage is called a pipeline
		// First pipeline in our example is with the use of $match
		// $match is used to pass the documents or get the documents that will pass our condition
			// syntax: {$match: {field : value}}

		// The second pipeline in our example is with the use of $group
		// $group grouped elements/documents together and create an analysis of these group documents
			// syntax: {$group: {_id: <id>, fieldResult: "valueResult"}}
			// _id: this is us associating an id for aggregated result
				// When id is provided with $supplier, we created aggregated result per supplier
				// $field will refer to a field name that is available in the documents that being aggregated on

		// $sum will get the total of all the values of the given field. This will allow us to get the total of the values of the given field per group. It will non-numerical values


// $sum - gets the sum of all value of the values in a given field

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id : "$supplier", totalStocks: {$sum : "$stocks"}}
	}
]);

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "$supplier", totalPrice: {$sum : "$price"}}
	}
]);


db.fruits.aggregate([
	{
		$match: {"supplier" : "Red Farms Inc."}
	},
	{
		$group: {_id: "$supplier", totalStocks: {$sum : "$stocks"}}
	}
]);

// $avg - average price of all items on sale

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "avgPriceOnSale", avgPrice: {$avg : "$price"}}
	}
]);

// $max - allow us to get the highest value out of all the values of a given field

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "$supplier", maxStock: {$max: "$stocks"}}
	}
])

// $min - gets the lowes value of the values in a given field

db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id: "minStickOnSale", maxStock: {$min: "$stocks"}}
	}
])

// other stage

	// $count - is a stage we can add after a match stage to count all items

	db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$count: "itemsOnSale"
		}
	])

	db.fruits.aggregate([
		{
			$match: {"price" : {$lt: 50}}
		},
		{
			$count: "priceLessThan50"
		}
	])

	db.fruits.aggregate([
		{
			$match: {"onSale" : false}
		},
		{
			$count: "itemsNotOnSale"
		}
	])

	// count total number of items per group

	db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id: "$supplier", noOfItems: {$sum: 1}}
		}
	])

	// you can have multiple field result per aggregated result

	db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id: "$supplier", noOfItems: {$sum : 1}, totalStocks: {$sum : "$stocks"}}
		}
	])
	
// $out - save/output your aggregated result from $group in a new collection

	db.fruits.aggregate([
		{
			$match: {"onSale" : true}
		},
		{
			$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
		},
		{
			$out: "totalStocksPerSupplier"
		}
	])